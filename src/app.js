import express from 'express';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import authMiddleware from './middlewares/auth.js';
import authRouter from './routes/auth.js';
import dokterRouter from './routes/dokter.js';
import jadwalRouter from './routes/jadwal.js';

dotenv.config();

export const app = express();
app.use(express.json());
app.use(cookieParser());

const router = express.Router();

app.use('/api', router);
router.use('/auth', authRouter);
router.use(authMiddleware);
router.use('/dokter', dokterRouter);
router.use('/jadwal', jadwalRouter);

app.listen(process.env.APP_PORT, () => {
  console.log(`Server berjalan pada port ${process.env.APP_PORT}`);
});
