import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

const secretKey = process.env.SECRET_KEY;
export const generateToken = (key) => {
  const token = jwt.sign(key, secretKey, { expiresIn: 3600 * 2 });
  return token;
};

export const verifyToken = (token) => {
  try {
    return jwt.verify(token.replace('Bearer ', ''), secretKey);
  } catch (err) {
    throw new Error('Maaf token tidak valid.');
  }
};
