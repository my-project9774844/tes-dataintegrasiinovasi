export const generateArrayJadwal = (payload, id_dokter) => {
  const start_date = new Date(payload.date_range.start_date);
  const end_date = new Date(payload.date_range.end_date);
  const days_of_week = [
    'minggu',
    'senin',
    'selasa',
    'rabu',
    'kamis',
    'jumat',
    'sabtu',
  ];
  let current_date = start_date;
  let jadwal = [];
  while (current_date <= end_date) {
    if (
      current_date.getDay() ===
      days_of_week.findIndex((val) => val === payload.day)
    ) {
      jadwal.push([
        payload.day,
        payload.time_start,
        payload.time_finish,
        payload.quota,
        payload.status,
        new Date(current_date),
        id_dokter,
      ]);
    }
    current_date.setDate(current_date.getDate() + 1);
  }
  const query = jadwal
    .map((_, rowIndex) => {
      return `(${jadwal[rowIndex]
        .map((_, colIndex) => {
          return `$${colIndex + rowIndex * jadwal[0].length + 1}`;
        })
        .join(', ')})`;
    })
    .join(', ');
  return [jadwal, query];
};
