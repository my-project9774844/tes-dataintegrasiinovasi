import express from 'express';
import { generateToken } from '../utils/jwt.js';

const router = express.Router();

router.get('/getToken', (req, res) => {
  const token = generateToken({
    expired: Math.floor(Date.now() / 1000) + 60 * 60,
  });
  return res.status(200).json({
    status: 'ok',
    message: 'berhasil',
    token: token,
  });
});

export default router;
