import express from 'express';
import pool from '../db.js';
import dotenv from 'dotenv';

dotenv.config();

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = `select * from ${process.env.DB_TABEL_DOKTER}`;
    const result = await pool.query(query);
    res.status(200).json({
      status: 'ok',
      message: 'berhasil',
      data: result.rows,
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      status: 'internal server error',
    });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const query = `select * from ${process.env.DB_TABEL_DOKTER} where id = $1`;
    const values = [req.params.id];
    const result = await pool.query(query, values);
    res.status(200).json({
      status: 'ok',
      message: result.rows.length > 0 ? 'berhasil' : 'Data tidak ditemukan',
      data: result.rows,
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      status: 'internal server error',
    });
  }
});

export default router;
