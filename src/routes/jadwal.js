import express from 'express';
import pool from '../db.js';
import dotenv from 'dotenv';
import { generateArrayJadwal } from '../utils/jadwal.js';

dotenv.config();

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const query_get_id_dokter = `select id from ${process.env.DB_TABEL_DOKTER} where nama=$1`;
    const value_get_id_dokter = [req.body.doctor_name];
    const id_dokter = await pool.query(
      query_get_id_dokter,
      value_get_id_dokter
    );
    if (id_dokter.rows.length <= 0) {
      return res.status(400).json({
        status: 'bad request',
        error: 'Dokter tidak ada',
      });
    }
    try {
      const [value_tambah_jadwal, query_insert] = generateArrayJadwal(
        req.body,
        id_dokter.rows[0].id
      );
      const query_tambah_jadwal = `insert into ${process.env.DB_TABEL_JADWAL} (hari, waktu_mulai, waktu_selesai, kuota, status, tanggal, id_dokter) values ${query_insert}`;
      await pool.query(query_tambah_jadwal, value_tambah_jadwal.flat());
      return res.status(201).json({
        status: 'created',
        message: 'Berhasil menambahkan jadwal baru',
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json({
        status: 'internal server error',
      });
    }
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      status: 'internal server error',
    });
  }
});

router.get('/', async (req, res) => {
  try {
    const query_get_jadwal = `select jd.id, jd.id_dokter as doctor_id, jd.hari as day, jd.waktu_mulai as time_start, jd.waktu_selesai as time_finish, jd.kuota as quota, jd.status, d.nama as doctor_name, jd.tanggal as date from ${process.env.DB_TABEL_JADWAL} jd inner join ${process.env.DB_TABEL_DOKTER} d on jd.id_dokter = d.id`;
    const result = await pool.query(query_get_jadwal);
    return res.status(200).json({
      status: 'ok',
      message: 'berhasil',
      data: result.rows,
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      status: 'internal server error',
    });
  }
});

export default router;
