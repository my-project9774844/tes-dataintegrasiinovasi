import { verifyToken } from '../utils/jwt.js';

const authToken = (req, res, next) => {
  const token = req.header('Authorization');
  if (!token)
    return res.status(400).json({
      status: 'bad request',
      error: 'Anda tidak memiliki token.',
    });

  try {
    verifyToken(token);
    next();
  } catch (err) {
    return res.status(401).json({
      status: 'unauthorized',
      error: 'Token tidak valid.',
    });
  }
};

export default authToken;
